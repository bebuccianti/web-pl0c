(ql:quickload :hunchentoot)
(ql:quickload :jsown)
(ql:quickload :inferior-shell)
(ql:quickload :quicklisp-slime-helper)

(defvar *server*
  (make-instance 'hunchentoot:easy-acceptor
                 :port 80
                 :document-root #p"/home/root/quicklisp/local-projects/web-pl0c/dist/"))

(defun main ()
  (hunchentoot:start *server*)
  (setf swank::*loopback-interface* "0.0.0.0")
  (swank:create-server :port 4005 :style swank:*communication-style* :dont-close t))

(defun compiler-output (filename)
  (inferior-shell:run/nil (format nil "objdump -M intel -d inputs/~A > inputs/~A.o" filename filename) :on-error nil)
  (inferior-shell:run/nil (format nil "tail -n +318 inputs/~A.o > inputs/bottom~A.txt" filename filename) :on-error nil)
  (with-open-file (stream (format nil "inputs/bottom~A.txt" filename))
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      (delete-file (format nil "inputs/~A.o" filename))
      (delete-file (format nil "inputs/bottom~A.txt" filename))
      data)))

(defun get-compiler-output (code)
  (let ((filename (gensym)))
    (with-open-file (str (format nil "inputs/~A.pl0" filename)
			 :direction :output
			 :if-exists :supersede
			 :if-does-not-exist :create)
      (format str "~A ~%" code))
    (let ((err (nth-value 2
			  (inferior-shell:run
			   (format nil "pl0c inputs/~A.pl0 > inputs/error~A.txt"
				   filename filename)
			   :on-error nil))))
      (if (= err 0)
	  (compiler-output filename)
	  (with-open-file (stream (format nil "inputs/error~A.txt" filename))
	    (let ((data (make-string (file-length stream))))
	      (read-sequence data stream)
              (delete-file (format nil "inputs/~A.pl0" filename))
	      (delete-file (format nil "inputs/error~A.txt" filename))
	      (subseq data (search ":" data))))))))

(hunchentoot:define-easy-handler (compiler :uri "/compilar"
                                           :default-request-type :post) ()
  (setf (hunchentoot:content-type*) "text/plain"
	(hunchentoot:header-out "Access-Control-Allow-Origin") "*"
	(hunchentoot:header-out "Access-Control-Allow-Headers") "access-control-allow-origin, content-type")
  (let ((request-type (hunchentoot:request-method hunchentoot:*request*)))
    (if (eq request-type :post)
        (let* ((code (hunchentoot:raw-post-data :force-text t))
               (code-parsed (jsown:parse code "code")))
          (get-compiler-output (jsown:val code-parsed "code")))
        nil)))

(main)
