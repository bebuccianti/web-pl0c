FROM archlinux/base:latest

RUN pacman -Sy --noconfirm && \
pacman --noconfirm -S sbcl binutils && \
curl "https://beta.quicklisp.org/quicklisp.lisp" > /tmp/quicklisp.lisp && \
export HOME=/home/root && \
sbcl --no-sysinit --no-userinit --non-interactive \
--load /tmp/quicklisp.lisp \
--eval "(quicklisp-quickstart:install)" \
--eval "(ql::without-prompting (ql:add-to-init-file :sbcl))"

WORKDIR /home/root/quicklisp/local-projects/web-pl0c
RUN mkdir inputs/
COPY webPl0c.lisp ./
ADD dist/ ./dist
ADD pl0c /usr/bin/
RUN chmod -R 775 dist/

CMD sbcl --load /home/root/quicklisp/setup.lisp --eval '(load "webPl0c.lisp")'

EXPOSE 80
EXPOSE 4005