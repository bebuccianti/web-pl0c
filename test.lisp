(hunchentoot:stop *server*)

(inferior-shell:run "pl0c inputs/G674.pl0 > inputs/erroresG674.txt")
(inferior-shell:run "cat inputs/erroresG674.txt")

(inferior-shell:run/nil "objdump -M intel -d inputs/G674 > inputs/G674.o")
(inferior-shell:run "cat inputs/G674.o")
(inferior-shell:run/nil "tail -n +318 inputs/G674.o > inputs/bottomG674.txt")
(inferior-shell:run "cat inputs/bottomG674.txt")

(jsown:parse "{\"code\":\"var x, y, z;.\"}" "code")
(jsown:val (jsown:parse "{\"code\":\"var x, y, z;.\"}" "code") "code")

(jsown:parse "{\"code\":{\"text\":\"var x, y, z;.\"}}" "code")
(jsown:parse "{\"foo\":\"bar\",\"frolic\":100,\"fragrance\":10.01,\"for\":\"markup\"}" "foo" "frolic" "fragrance")
